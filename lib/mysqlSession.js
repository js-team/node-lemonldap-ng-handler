(function() {
  var DBISession, MySQLSession, convert;

  DBISession = require('./dbiSession');

  convert = {
    database: 'database',
    dbname: 'database',
    host: 'host',
    port: 'port'
  };

  MySQLSession = class MySQLSession extends DBISession {
    constructor(logger, opts) {
      var dbargs, dbiargs, i, k, len, t, t2, tmp;
      if (opts.DataSource.match(/^dbi:mysql:(.*$)/)) {
        dbiargs = RegExp.$1;
        tmp = dbiargs.split(/;/);
        dbargs = {
          user: opts.UserName,
          password: opts.Password
        };
        for (i = 0, len = tmp.length; i < len; i++) {
          t = tmp[i];
          if (t.match(/=/)) {
            t2 = t.split(/=/);
            if (k = convert[t2[0]]) {
              dbargs[k] = t2[1];
            }
          } else {
            dbargs.database = t;
          }
        }
        super('mysql', logger, dbargs);
      } else {
        logger.error('Bad DataSource');
      }
    }

  };

  module.exports = MySQLSession;

}).call(this);
